
NAME = game_2048

SRCS =	main.c ft_auxi.c ft_auxi_01.c\
	ft_error.c\
	ft_game.c

SRCO = $(SRCS:.c=.o)

all: $(NAME)

$(NAME):
	make -C libft/ fclean
	make -C libft/
	gcc -Wall -Werror -Wextra -I libft/ -c $(SRCS)
	gcc -o $(NAME) $(SRCO) -L libft/ -lft

clean:
	make -C libft/ clean
	rm -rf $(SRCO)

fclean: clean
	make -C libft/ fclean
	rm -rf $(NAME)

re: fclean all
