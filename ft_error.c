
#include "game_2048.h"

int		ft_error(t_game *g)
{
	if (g->size < 4 || g->size > 10)
		return (1);
	if (!ft_puissance(2, g->max_val) || g->max_val > 2048)
		return (1);
	return (0);
}
