
#include "game_2048.h"

int		main(int ac, char **av)
{
	t_game	g;
	time_t	t;

	(void)av;
	srand((unsigned int)time(&t));
	if (ac == 1)
	{
		ft_init_game(&g, GRILL_SIZE);
		if (!ft_error(&g))
			ft_game(&g);
		else
			ft_usage(0);
	}
	else
		ft_usage(1);
	return (0);
}
